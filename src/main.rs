#![feature(const_fn,core,no_std)]

extern crate core;

pub mod client;
pub mod clock;
pub mod timer;
pub mod event;

use core::cell::{RefCell};
use event::{Event, EventGenerator, EventTarget};
use clock::{Clock,PosixClock};
use timer::{BasicTimer, TimerClient};

fn handle_event<E: EventGenerator>(event: &mut Event<E>,
                          dependents: &mut [&mut EventTarget<E>]) {
    if event.has_event() {
        let id = event.id();
        for d in dependents {
            d.fired(id);
        }
    }
}

pub fn main() {
    use client::Beeper;

    let clock_event = RefCell::new(Event::new(clock::PosixClockNum::PosixClock1));
    let clock = RefCell::new(clock::PosixClock::new(&clock_event));
    let timer_event = RefCell::new(Event::new(()));
    let timer = RefCell::new(BasicTimer::new(&clock, &timer_event));


    let beeper = RefCell::new(Beeper::new(&timer));

    loop {
        handle_event(&mut *clock_event.borrow_mut(), &mut [&mut *timer.borrow_mut()]);
        handle_event(&mut *timer_event.borrow_mut(), &mut [&mut *beeper.borrow_mut()]);

        // Just simulate a hardware clock ticking. In a real system this would
        // be taken care of elsewhere
        clock.borrow_mut().tick();
    }


}

