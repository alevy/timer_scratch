pub trait EventGenerator {
    type EventId;
}

pub trait EventTarget<E: EventGenerator> {
    fn fired(&mut self, event: &E::EventId);
}

pub struct Event<E: EventGenerator> {
    evs: usize,
    id: E::EventId
}

impl<E: EventGenerator> Event<E> {
    pub const fn new(id: E::EventId) -> Event<E> {
        Event{ evs: 0, id: id }
    }

    pub fn has_event(&mut self) -> bool {
        if self.evs > 0 {
            self.evs -= 1;
            return true;
        }
        return false;
    }

    pub fn id(&mut self) -> &E::EventId {
        &self.id
    }

    pub fn dispatch(&mut self) {
        self.evs += 1;
    }
}

