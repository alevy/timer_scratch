use core::cell::RefCell;
use timer::*;
use event::{EventGenerator, EventTarget};

#[allow(dead_code)]
pub struct Beeper<'a, T: Timer + 'a> {
    timer: &'a RefCell<T>
}

impl<'a, T: Timer + 'a> Beeper<'a, T> {
    pub fn new(timer: &'a RefCell<T>) -> Beeper<'a, T> {
        timer.borrow_mut().repeat(1000000);
        Beeper {
            timer: timer
        }
    }

}

impl<'a, T: EventGenerator + Timer + 'a> EventTarget<T> for Beeper<'a, T> {
    fn fired(&mut self, _: &T::EventId) {
        println!("Beeper: beep!");
    }
}

