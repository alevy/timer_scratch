use core::cell::RefCell;
use clock::*;
use event::{Event, EventGenerator, EventTarget};

pub trait Timer {
    fn oneshot(&mut self, interval: u64);
    fn repeat(&mut self, interval: u64);
    fn cancel(&mut self);
}

pub trait TimerClient<T: Timer + EventGenerator> {
    fn fired(&mut self, _event: &T::EventId);
}

pub struct BasicTimer<'a, A: Clock + 'a> {
    interval: u64,
    repeat: bool,
    event: &'a RefCell<Event<BasicTimer<'a, A>>>,
    clock: &'a RefCell<A>
}

impl<'a, C: Clock> EventGenerator for BasicTimer<'a, C> {
    type EventId = ();

}

impl<'a, A: Clock> BasicTimer<'a, A> {
    pub fn new(clock: &'a RefCell<A>, event: &'a RefCell<Event<Self>>) -> BasicTimer<'a, A> {
        BasicTimer {
            interval: 0, repeat: false, event: event, clock: clock
        }
    }
}

impl<'a, A: Clock> Timer for BasicTimer<'a, A> {
    fn oneshot(&mut self, interval: u64) {
        self.interval = interval;
        self.repeat = false;
        self.clock.borrow_mut().set_alarm(interval);
    }

    fn repeat(&mut self, interval: u64) {
        self.interval = interval;
        self.repeat = true;
        self.clock.borrow_mut().set_alarm(interval);
    }

    fn cancel(&mut self) {
        self.repeat = false;
    }
}

impl<'a, A: Clock + EventGenerator> EventTarget<A> for BasicTimer<'a, A> {
    fn fired(&mut self, _event: &A::EventId) {
        if self.repeat {
            self.clock.borrow_mut().set_alarm(self.interval);
        }
        println!("Timer: fired");
        self.event.borrow_mut().dispatch();
    }
}

