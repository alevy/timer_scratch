//! Trait and prototype implementation for clocks.
//!
//! A `Clock` keeps track of time in hardware specific units. It can be
//! configured to fire an alarm when the clock reaches a certain value.
//!
//! This module defines a prototype `Clock` called `PosixClock` that uses system
//! time in seconds.

use event::{Event, EventGenerator};
use core::cell::RefCell;

/// A clock keeps track of the time
pub trait Clock {
    /// Gets the current time in units specific to the clock.
    fn now(&self) -> u64;

    /// Fire an alarm at a the given time (in units corresponding to the return
    /// value from `now`).
    ///
    /// When the alarm is fired, and a  corresponding `Event` will be
    /// dispatched.
    fn set_alarm(&mut self, time: u64);

    /// Called by the "hardware" when the internal clock should be incremented
    /// by a tick
    fn tick(&mut self);
}


pub struct PosixClock<'a> {
    alarm: u64,
    event: &'a RefCell<Event<PosixClock<'a>>>
}

impl<'a> PosixClock<'a> {
    pub const fn new(event: &'a RefCell<Event<PosixClock<'a>>>) -> PosixClock<'a> {
        PosixClock { alarm: 0, event: event }
    }
}

impl<'a> EventGenerator for PosixClock<'a> {
    type EventId = PosixClockNum;
}

impl<'a> Clock for PosixClock<'a> {
    fn now(&self) -> u64 {
        0
    }

    // This is a simple prototype. In practice, such a driver would simply
    // configure a hardware clock, and an interrupt will arrive
    // asyncronously, without any threading. `posix_clock1_fired` would
    // simply be an extern function configured in the chip's interrupt
    // table.
    //
    // In this prototype, it's technically possible to have multiple outstanding
    // timers, but in real hardware this would not be the case.
    fn set_alarm(&mut self, time: u64) {

        // this seems a bit circular (why not just pass in the interval?). In
        // real hardware, you set the alarm by writing the specific clock value
        // when you want the alarm to fire. This is just an appromixation of the
        // interface.
        self.alarm = time - self.now();
    }

    fn tick(&mut self) {
        if self.alarm > 0 {
            self.alarm -= 1;
            if self.alarm == 0 {
                self.event.borrow_mut().dispatch();
            }
        }
    }
}

pub enum PosixClockNum {
    PosixClock1
}


